const { successResponse } = require('../../utils/serializer');
const schema = require('./schema');
const addressSchema = require('../address/schema');
const userDetailSchema = require('../userDetail/schema');

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const getAllUser = async (req, res) => {
    // const data = await schema.find({}).limit(10).skip(10);
    const data = await schema.find({});
    return res.send(successResponse(data));
}

const loginUser = async (req, res) => {
    const { username, password } = req.body;
    const user = await schema.findOne({
        username: username
    }).exec();

    if (!user) {
        return res.status(401).send('User not found');
    }

    const passwordMatch = await bcrypt.compare(password, user.password);

    if (!passwordMatch) {
        return res.status(401).send('Username or password is incorrect');
    }

    const token = jwt.sign({ userId: user._id }, 'EXPENSE_TRACKER', {
        expiresIn: '30d',
    });

    return res.send(successResponse({ user, token }, 200, 'Authenticated'));

}

const createUser = async (req, res) => {
    const userData = req.body;
    const hashedPassword = await bcrypt.hash(userData.password, 10);
    let userDetail = null;
    if (req.body.details) {

        const address = await addressSchema.create({
            "country": req.body.address.country,
            "state": req.body.address.state,
            "city": req.body.address.city
        })

         userDetail = await schema.create({
            "firstName": req.body.details.firstName,
            "lastName": req.body.details.lastName,
            "occupation": req.body.details.occupation,
            "gender": req.body.details.gender,
            "address": address._id
        });

    }
    const data = await schema.create({
        email: req.body.email, 
        username: req.body.username,
        phone: req.body.phone,
        userDetails: userDetail?._id,
        password: hashedPassword
    });

    return res.send(successResponse('', 201, 'Created'));
}

const updateUser = async (req, res) => {
    const data = await schema.findByIdAndUpdate(req.params.id, {
        ...req.body
    });

    return res.send(successResponse('', 200, 'Updated'));
}

const deleteUser = async (req, res) => {
    await schema.findByIdAndDelete(req.params.id);
    return res.send(successResponse('', 200, 'Deleted'));
}

const getUserById = async (req, res) => {
    const data = await schema.findById(req.params.id);
    return res.send(successResponse(data, 200, 'OK'));
}

module.exports = {
    getAllUser,
    createUser,
    updateUser,
    deleteUser,
    loginUser,
    getUserById,
}