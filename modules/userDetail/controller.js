const { successResponse } = require('../../utils/serializer');
const schema = require('./schema');
const userSchema = require('../user/schema');
const addressSchema = require('../address/schema');

const getAllUserDetail = async (req, res) => {
    // const data = await schema.find({}).limit(10).skip(10);
    const data = await schema.find({});
    return res.send(successResponse(data));
}

const createUserDetail = async (req, res) => {
    console.log(req.body);
    // return res.send(req.user);

    

    const address = await addressSchema.create({
        "country": req.body.address.country,
        "state": req.body.address.state,
        "city": req.body.address.city
    })

    const data = await schema.create({
        "firstName": req.body.firstName,
        "lastName": req.body.lastName,
        "occupation": req.body.occupation,
        "gender": req.body.gender,
        "address": address._id
    });

    await userSchema.findByIdAndUpdate('66040a54e051151b76561d8b', {
        userDetails: data._id
    });
    // await userSchema.

    return res.send(successResponse(data, 201, 'Created'));
}

const updateUserDetail = async (req, res) => {
    const data = await schema.findByIdAndUpdate(req.params.id, {
        ...req.body
    });

    return res.send(successResponse('', 200, 'Updated'));
}

const deleteUserDetail = async (req, res) => {
    await schema.findByIdAndDelete(req.params.id);
    return res.send(successResponse('', 200, 'Deleted'));
}

const getUserDetailById = async (req, res) => {
    const data = await schema.findById(req.params.id).populate('address');
    return res.send(successResponse(data, 200, 'OK'));
}

module.exports = {
    getAllUserDetail,
    createUserDetail,
    updateUserDetail,
    deleteUserDetail,
    getUserDetailById,
}