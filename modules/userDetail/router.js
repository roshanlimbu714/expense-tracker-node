const express = require('express');
const router = express.Router();
const controller = require('./controller');
const verifyToken = require('../authentication/auth.middleware');

router.post('', verifyToken, controller.createUserDetail, );
router.put('/:id', controller.updateUserDetail);
router.get('/:id', controller.getUserDetailById);
router.get('', controller.getAllUserDetail);
router.delete('/:id', controller.deleteUserDetail);


module.exports = router;
