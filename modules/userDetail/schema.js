const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const UserSchema = new Schema({
    firstName: {
        type: String,
    },
    lastName: {
        type: String,
    },
    img: {
        type: String,
        required:false,
    },
    address:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Address',
        required:false
    },
    dateOfBirth:{ 
        type: Date,
        default: new Date()
    },
    occupation: {
        type: String,
        required: false,
    },
    gender: {
        type: String,
        enum : ['MALE','FEMALE','OTHERS'],
    }
}, { timestamps: true });


// Export model
module.exports = mongoose.model("UserDetail", UserSchema);
