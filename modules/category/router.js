const express = require('express');
const router = express.Router();
const controller = require('./controller');
const verifyToken = require('../authentication/auth.middleware')
router.post('', controller.createCategory);
router.put('/:id', controller.updateCategory);
router.get('/:id', controller.getCategoryById);
router.get('',verifyToken, controller.getAllCategory, );
router.delete('/:id', controller.deleteCategory);


module.exports = router;
