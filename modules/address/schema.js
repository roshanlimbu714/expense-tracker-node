const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const UserSchema = new Schema({
    country: {
        type: String,
        required:true
    },
    state: {
        type: String,
        required:true
    },
    city:{
        type: String,
        required:true
    },
    addressLine1: {
        type: String,
        default:''
    },
    addressLine2: {
        type: String,
        default:''
    },
}, { timestamps: true });


// Export model
module.exports = mongoose.model("Address", UserSchema);
